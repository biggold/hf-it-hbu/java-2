package pruefung1.aufgabe3;

public abstract class Hotel extends Unterkunft {
    private int sterne;

    public Hotel(String ort, int sterne) {
        super(ort);
        this.sterne = sterne;
    }

    public abstract void essen();

    public int getSterne() {
        return sterne;
    }
}
