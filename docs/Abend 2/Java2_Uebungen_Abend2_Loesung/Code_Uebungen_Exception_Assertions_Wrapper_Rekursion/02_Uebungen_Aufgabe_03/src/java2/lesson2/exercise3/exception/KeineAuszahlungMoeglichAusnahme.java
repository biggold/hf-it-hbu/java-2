package java2.lesson2.exercise3.exception;

/**
 * Exception Klasse f�r den Fall, dass keine Auszahlung m�glich ist
 * 
 * @author parz
 * @version 1.0
 * 
 */
public class KeineAuszahlungMoeglichAusnahme extends Exception {

	public KeineAuszahlungMoeglichAusnahme() {
		super();
	}

	public KeineAuszahlungMoeglichAusnahme(String msg) {
		super(msg);
	}
}
