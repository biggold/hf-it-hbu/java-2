package abend2.uebung2;

/**
 * Aufgabenstellung
 * Implementiere eine Methode, der ein Array und ein Index übergeben wird und danach das Element an der Stelle Index im Array zurückgegeben wird.
 * Falls der Methode ein ungültiger Index übergegeben wird (d.h. der Index liegt ausserhalb der Array Grenzen), wird beim Zugriff auf dieses Element im Array automatisch eine Exception vom Typ java.lang.ArrayIndexOutOfBoundsExceptiongeworfen.
 * Erweitere den Code um eine try-catch-Klausel, um diese Ausnahme abzufangen und stattdessen die Meldung "index XX liegt ausserhalb der Array-Grenze" auszugeben.
 */

public class ArrayAccess {
    public static int accessArray(int[] array, int index) {
        int tmp = -1;
        try {
            return array[index];
        } catch (java.lang.ArrayIndexOutOfBoundsException e) {
            System.out.println("Index out of Array range");
            e.printStackTrace();
        }
        return tmp;
    }

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        accessArray(array, 11);
    }
}
