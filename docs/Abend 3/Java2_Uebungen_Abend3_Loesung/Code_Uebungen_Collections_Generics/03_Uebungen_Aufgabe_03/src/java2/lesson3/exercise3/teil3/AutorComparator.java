package java2.lesson3.exercise3.teil3;

import java.util.Comparator;

public class AutorComparator implements Comparator<Buch> {

	public int compare(Buch b1, Buch b2) {
		return b1.getAutor().compareTo(b2.getAutor());
	}
}
