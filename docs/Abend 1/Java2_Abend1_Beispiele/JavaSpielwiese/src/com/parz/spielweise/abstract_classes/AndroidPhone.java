package com.parz.spielweise.abstract_classes;

abstract class AndroidPhone implements MobilePhone {
    public final void displayPhoneOSType() {
        System.out.println("I run on Android OS");
    }
 
    public final void supportAndroidApps() {
        System.out.println("I support Android Apps (File Format .apk)");
    }
    
    public abstract void capturePhoto();
    
    public abstract void supportAndroidAppStore();
}
