package pruefung1.aufgabe3;


public class Appartement extends Unterkunft {
    private String name;
    private int nummber;

    public Appartement(String ort, String name, int nummer) {
        super(ort);
        this.name = name;
        this.nummber = nummer;
    }

    @Override
    public void uebernachten() {
        System.out.println("Das Appartement heisst " + this.name + ", hat die Nummer " + this.nummber + " und ist in " + this.getOrt());
    }
}
