package java2.lesson3.exercise2;

public class Koordinate {

	private Integer x;
	private Integer y;

	public Koordinate(Integer x, Integer y) {
		this.x = x;
		this.y = y;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return ("x = " + x + " , y = " + y);
	}
}

