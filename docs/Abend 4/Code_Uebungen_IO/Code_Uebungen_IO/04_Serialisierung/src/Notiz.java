import java.io.Serializable;

/**
 * Serialisierbare Notiz Klasse
 * @author parz
 *
 */
public class Notiz implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String autor;
	private String nachricht;

	public Notiz(String autor, String nachricht) {
		super();
		this.autor = autor;
		this.nachricht = nachricht;
	}

	/**
	 * @return the autor
	 */
	public String getAutor() {
		return autor;
	}

	/**
	 * @param autor
	 *            the autor to set
	 */
	public void setAutor(String autor) {
		this.autor = autor;
	}

	/**
	 * @return the nachricht
	 */
	public String getNachricht() {
		return nachricht;
	}

	/**
	 * @param nachricht
	 *            the nachricht to set
	 */
	public void setNachricht(String nachricht) {
		this.nachricht = nachricht;
	}
}
