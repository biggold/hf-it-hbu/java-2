package com.parz.spielweise.abstract_classes;

public class HuawaiPhone extends AndroidPhone{
    @Override
    public void capturePhoto() {
        System.out.println("Huawai can capture photo");
    }
    
    public void supportAndroidAppStore() {
    	System.out.println("Huawai supports Huawai AppGallery");
    }
 
    @Override
    public void makeACall(Long number, Integer countryCode) {
        System.out.println(String.format("Huawai can make audio and video calls to 0%d-%d", countryCode, number));
    }
 
    @Override
    public boolean sendSMS(String message) {
        System.out.println(String.format("Huawai can send text message - %s", message));
        return true;
    }
}
