package java2.lesson3.exercise3.teil2;

import java.util.ArrayList;
import java.util.Collections;

// Aufgaben c) - e)
public class Bibliothek {

	ArrayList<Buch> buecherListe = new ArrayList<Buch>();

	public Bibliothek() {

	}

	public void init() {
		buecherListe.add(new Buch("The Lord of the Rings", "J.R.R. Tolkien",
				"English", 1184));
		buecherListe.add(new Buch("The Hitchhiker's Guide to the Galaxy",
				"Douglas Adams", "English", 224));
		buecherListe.add(new Buch("Ichi kyu hachi yon", "Haruki Murakami",
				"Japanisch", 1050));
		buecherListe.add(new Buch(
				"Der Hundertjährige, der aus dem Fenster stieg und verschwand",
				"Jonas Jonasson", "Deutsch", 567));
		buecherListe.add(new Buch("Head First Java", "Kathy Sierra", "English",
				688));

	}

	public void addBuch(Buch buch) {
		buecherListe.add(buch);
	}

	public void printListe() {
		for (Buch buch : buecherListe) {
			System.out.println(buch.getTitel()+" "+buch.getAutor()+" "+buch.getSprache()+" "+buch.getSeitenzahl());
		}
	}

	public void sortiereBuecher() {
		Collections.sort(buecherListe);
	}

	public static void main(String[] args) {
		Bibliothek bibliothek = new Bibliothek();
		bibliothek.init();
		bibliothek.addBuch(new Buch("Harry Potter", "J.K. Rowling", "English", 650));
		bibliothek.sortiereBuecher();
		bibliothek.printListe();
	}

}
