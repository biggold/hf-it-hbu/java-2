package pruefung1.aufgabe3;

public class Hilton extends Hotel {
    public Hilton(String ort, int sterne) {
        super(ort, sterne);
    }

    @Override
    public void essen() {

    }

    @Override
    public void uebernachten() {
        System.out.println("Schlafen im " + this.getSterne() + " Sterne Hilton Hotel in " + this.getOrt() + " ist super");
    }
}
