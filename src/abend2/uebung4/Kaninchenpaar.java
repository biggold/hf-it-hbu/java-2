package abend2.uebung4;

public class Kaninchenpaar {
    public static void main(String[] args) {
        for (int i = 0; i <= 12; i++) {
            System.out.println("Kaninchenpaar (Monat: "+i+") : " + Kaninchenpaar.nextGen(i));
        } }
        public static int nextGen(int n) {
            if (n < 2){
                return 1;
            } else {
                System.out.println("DEBUG: nextgen(n -1): " + (n -1) + " nextgen(n-2): " + (n-2));
                return nextGen(n - 1) + nextGen(n - 2);
            }
        }
}
