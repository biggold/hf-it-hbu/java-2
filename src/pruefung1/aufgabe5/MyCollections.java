package pruefung1.aufgabe5;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class MyCollections {
    public static void main(String[] args) {
        Set<Integer> mySet = new TreeSet<Integer>(new IntegerComparator());
        mySet.add(111);
        mySet.add(9);
        mySet.add(32);
        mySet.add(89);

        for (Integer integer : mySet) System.out.println(integer);
    }
}
