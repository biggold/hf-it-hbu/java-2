package pruefung1.aufgabe4;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.io.IOException;

public class NumberCount {
    public static void main(String[] args) {
        Scanner scannerFile = null;

        try {
            scannerFile = new Scanner(new File("text.txt"));
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            System.out.println("Put the file in teh current working dire which is:" + System.getProperty("user.dir"));
            e.printStackTrace();
        }

        int numberCount = 0;
        // Loop until no more numbers to be read from file
        while (scannerFile.hasNext()) {
            // Read next number from file
            if (scannerFile.hasNextInt()) {
                numberCount++;
                System.out.println("Found nummber: " + scannerFile.nextInt() + ", Current count is: " + numberCount);
            } else {
                scannerFile.next();
            }
        }
        scannerFile.close();


        // We create the new dir in the execution dir
        try {
            Path path = Paths.get(System.getProperty("user.dir")+"/out");
            Files.createDirectories(path);
            System.out.println("Directory is created! Path: " + path.toString());
        } catch (IOException e) {
            System.err.println("Failed to create directory!" + e.getMessage());
        }

        try {
            FileWriter myWriter = new FileWriter(System.getProperty("user.dir")+"/out/numberCount.txt");
            myWriter.write("Counted Numbers: " + numberCount);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }
}
