package java2.lesson2.exercise3;

import java2.lesson2.exercise3.exception.KeinAbzugMoeglichAusnahme;
import java2.lesson2.exercise3.exception.KeineAuszahlungMoeglichAusnahme;

/**
 * Klasse f�r Berechnungen der 3. S�ule im Kanton St. Gallen
 * 
 * @author parz
 * @version 1.1
 * 
 */
public class Saeule3aSG implements ISaeule3a {

	/**
	 * Konstante Kapital Steuerfuss im Kanton Z�rich
	 */
	private static final double KAPITAL_STEUERFUSS_SG = 0.0875;

	/**
	 * Berechnung der Steuerersparnis, wenn in die S3a einbezahlt wird.
	 * 
	 * @param steuerbaresEinkommen
	 * @param steuerbaresVermoegen
	 * @param einzahlungS3a
	 * @return
	 * @throws KeinAbzugMoeglichAusnahme
	 */
	public int berechneSteuerErsparnis(int steuerbaresEinkommen,
			int steuerbaresVermoegen, int einzahlungS3a)
			throws KeinAbzugMoeglichAusnahme {
		if (steuerbaresEinkommen > 0 && steuerbaresVermoegen > 0) {
			return (int) (einzahlungS3a / 3.9);
		} else {
			throw new KeinAbzugMoeglichAusnahme(
					"Einkommen oder Vermoegen ist ungueltig!");
		}
	}

	/**
	 * Berechnung der Kapitalsteuern, wenn das Kapital der S3a ausbezahlt wird.
	 * 
	 * @param kapitalS3a
	 * @return
	 * @throws KeineAuszahlungMoeglichAusnahme
	 */
	public int berechneKapitalSteuern(int kapitalS3a)
			throws KeineAuszahlungMoeglichAusnahme {
		if (kapitalS3a >= 0) {
			return (int) (kapitalS3a * KAPITAL_STEUERFUSS_SG);
		} else {
			throw new KeineAuszahlungMoeglichAusnahme("Kapital ist ungueltig!");
		}
	}
}
