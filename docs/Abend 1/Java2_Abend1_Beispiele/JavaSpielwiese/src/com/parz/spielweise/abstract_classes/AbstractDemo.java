package com.parz.spielweise.abstract_classes;

public class AbstractDemo {

    public static void main(String[] args) {
        System.out.println("This class demonstrates the use of abstract class in Java");
        System.out.println("----------------------------------------------------------");
        AndroidPhone samsung = new SamsungPhone();
        
        samsung.makeACall(1234567890L, 91);
        samsung.sendSMS("Hello, world!!!!");
        samsung.displayPhoneOSType();
        samsung.supportAndroidApps();
        samsung.capturePhoto();
        samsung.supportAndroidAppStore();
        System.out.println("----------------------------------------------------------");
        AndroidPhone huawai = new HuawaiPhone();
        
        huawai.makeACall(1234567890L, 91);
        huawai.sendSMS("Hello, world!!!!");
        huawai.displayPhoneOSType();
        huawai.supportAndroidApps();
        huawai.capturePhoto();
        huawai.supportAndroidAppStore();
        System.out.println("----------------------------------------------------------");
        ApplePhone apple = new ApplePhone();
        
        apple.makeACall(1234567890L, 91);
        apple.sendSMS("Hello, world!!!!");
        apple.displayPhoneOSType();
        apple.supportiOSApps();
        apple.capturePhoto();
        System.out.println("----------------------------------------------------------");
    }

}
