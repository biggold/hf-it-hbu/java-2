package java2.lesson4.exercise2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;

public class CreateCopyDelete {

	public static void main(String[] args) {
		try {
			Path path_1 = Paths.get("C:"+File.separator+"tmp"+File.separator+"datei_1.txt");
			Path path_2 = Paths.get("C:"+File.separator+"tmp"+File.separator+"datei_2.txt");
			// 1. Datei erzeugen
			Files.createFile(path_1);
			// 1. Datei fuellen
			// Zeilenumbruch:
			// \n ist unix
			// \r ist mac
			// \r\n ist windows format
			// System.getProperty("line.separator") funktioniert immer
			Files.write(path_1,
					"Ein wenig Text.\r\nNoch mehr Text.".getBytes(),
					StandardOpenOption.WRITE);
			// 1. -> 2. Datei kopieren
			Files.copy(path_1, path_2, StandardCopyOption.REPLACE_EXISTING);
			// 1. Datei loeschen
			Files.delete(path_1);
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}

	}

}
