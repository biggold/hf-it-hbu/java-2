package com.parz.spielweise.interfaces;

import java.io.Serializable;

public class SmartPhone implements MobilePhone, Serializable, InternetProtocol, Wireless, Bluetooth {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    public void makeACall(Long number, Integer countryCode) {
        System.out.println(String.format("I can make audio and video call to %d-%d", countryCode, number));
    }
 
    @Override
    public boolean sendSMS(String message) {
        System.out.println("I can send SMS");
        return true;
    }

	@Override
	public void discoverAgent() {
		System.out.println("I have discovered the Agent for the Smartphone");	
	}

	@Override
	public void registerNode() {
		System.out.println("I have registered the Node for the Smartphone");	
	}

	@Override
	public void createTunneling() {
		System.out.println("I have created the Tunneling for the Smartphone");	
	}

	@Override
	public void switchOnBluetooth() {
		System.out.println("Bluetooth is switched On");
		
	}

	@Override
	public void switchOffBluetooth() {
		System.out.println("Bluetooth is switched Off");
		
	}

	@Override
	public void switchOnWiFi() {
		System.out.println("WiFi is switched On");	
	}

	@Override
	public void switchOffWiFi() {
		System.out.println("WiFi is switched Off");		
	}
 
    public void canCapturePhoto() {
        System.out.println("I can take HD photo");
    }
    
    public void supportApps() {
    	System.out.println("I can support Apps");
    }
    
}
