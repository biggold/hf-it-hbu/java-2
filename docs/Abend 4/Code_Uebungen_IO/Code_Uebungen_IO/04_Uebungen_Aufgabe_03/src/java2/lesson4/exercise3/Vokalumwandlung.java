package java2.lesson4.exercise3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Vokalumwandlung {

	private static char[] aeiouLower = { 'a', 'e', 'i', 'o', 'u' };
	private static char[] aeiouUpper = { 'A', 'E', 'I', 'O', 'U' };

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.print("Name der Input Datei: ");
		String inputFileName = sc.next();
		System.out.print("Vokal: ");
		char vowel = sc.next().charAt(0);
		sc.close();
		
		File inputFile, outputFile;
		BufferedReader br = null;
		PrintWriter pw = null;
		try {
			inputFile = new File(inputFileName);
			outputFile = new File(createOutputFilename(inputFileName));
			br = new BufferedReader(new FileReader(inputFile));
			pw = new PrintWriter(new BufferedWriter(new FileWriter(outputFile)));
			String line;
			while ((line = br.readLine()) != null) {
				line = replaceVowels(line, vowel);
				pw.println(line);
				System.out.println(line);
			}
		} catch (FileNotFoundException fnfe) {
			System.out.println("Die Datei " + inputFileName
					+ " existiert nicht");
			return;
		} catch (IOException e) {
			System.out.println(e);
			return;
		} finally {
			try {
				if (pw != null)
					pw.close();
				if (br != null)
					br.close();
			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
		}

	}

	private static String replaceVowels(String textLine, char vowel) {
		char vowelLower = Character.toLowerCase(vowel);
		char vowelUpper = Character.toUpperCase(vowel);
		for (int i = 0; i < aeiouLower.length; i++) {
			textLine = textLine.replace(aeiouLower[i], vowelLower);
			textLine = textLine.replace(aeiouUpper[i], vowelUpper);
		}

		return textLine;
	}

	private static String createOutputFilename(String inputFilename) {
		return filename(inputFilename) + "_output." + extension(inputFilename);
	}

	private static String extension(String file) {
		int dot = file.lastIndexOf(".");
		return file.substring(dot + 1);
	}

	private static String filename(String file) { // gets filename without
													// extension
		int dot = file.lastIndexOf(".");
		int sep = file.lastIndexOf(File.separator);
		return file.substring(sep + 1, dot);
	}

}
