package com.parz.spielweise.interfaces;

public interface Wireless {
	void switchOnWiFi();
	void switchOffWiFi();
}
