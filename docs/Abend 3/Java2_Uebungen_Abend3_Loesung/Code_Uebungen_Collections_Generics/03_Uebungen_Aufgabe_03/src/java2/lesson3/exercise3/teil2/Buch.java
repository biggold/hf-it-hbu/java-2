package java2.lesson3.exercise3.teil2;

public class Buch implements Comparable<Buch> {

	private String titel;
	private String autor;
	private String sprache;
	private int seitenzahl;

	public Buch(String titel, String autor, String sprache, int seitenzahl) {
		this.titel = titel;
		this.autor = autor;
		this.sprache = sprache;
		this.seitenzahl = seitenzahl;
	}

	/**
	 * @return the titel
	 */
	public String getTitel() {
		return titel;
	}

	/**
	 * @param titel
	 *            the titel to set
	 */
	public void setTitel(String titel) {
		this.titel = titel;
	}

	/**
	 * @return the autor
	 */
	public String getAutor() {
		return autor;
	}

	/**
	 * @param autor
	 *            the autor to set
	 */
	public void setAutor(String autor) {
		this.autor = autor;
	}

	/**
	 * @return the sprache
	 */
	public String getSprache() {
		return sprache;
	}

	/**
	 * @param sprache
	 *            the sprache to set
	 */
	public void setSprache(String sprache) {
		this.sprache = sprache;
	}

	/**
	 * @return the seitenzahl
	 */
	public int getSeitenzahl() {
		return seitenzahl;
	}

	/**
	 * @param seitenzahl
	 *            the seitenzahl to set
	 */
	public void setSeitenzahl(int seitenzahl) {
		this.seitenzahl = seitenzahl;
	}

	// Aufgabe e)
	public int compareTo(Buch b) {
		return titel.compareTo(b.getTitel());
	}
}
