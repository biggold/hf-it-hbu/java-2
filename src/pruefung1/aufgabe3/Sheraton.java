package pruefung1.aufgabe3;

public class Sheraton extends Hotel {
    public Sheraton(String ort, int sterne) {
        super(ort, sterne);
    }

    @Override
    public void essen() {

    }

    @Override
    public void uebernachten() {
        System.out.println("Ich wohne im " + this.getSterne() + " Sterne Sheraton Hotel in " + this.getOrt());
    }
}
