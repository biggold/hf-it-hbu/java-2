package com.parz.spielwiese.wrapperklassen;

import java.util.ArrayList;
import java.util.List;

public class Wrapperklassen {

	public static void main(String[] args) {
		List <Integer> values = new ArrayList<Integer>();
		values.add(32800);
		values.add(99);
		values.add(1000000000);
		
		for (Integer x : values) {
			System.out.println(x);
		}
		
		// Vergleich Wrapperklasse vs. Klasse
		System.out.println();
		Wrapperklassen w = new Wrapperklassen();
		Integer i = new Integer(2);
		
		System.out.println("Wrapperklasse");
		System.out.println(i);
		w.add(i);
		System.out.println(i);
		
		System.out.println();
		System.out.println("Normale Klasse");
		
		MyInteger myInt = new MyInteger(2);
		System.out.println(myInt);
		w.add(myInt);
		System.out.println(myInt);
	}
	
	public void add(Integer a) {
		a += 2;
	}
	
	public void add(MyInteger a) {
		a.a += 2;
	}

}
