package com.parz.spielweise.abstract_classes;

public class SamsungPhone extends AndroidPhone {
    @Override
    public void capturePhoto() {
        System.out.println("Samsung can capture photo");
    }
    
    public void supportAndroidAppStore() {
    	System.out.println("Samsung supports Google Playstore");
    }
 
    @Override
    public void makeACall(Long number, Integer countryCode) {
        System.out.println(String.format("Samsung can make audio and video calls to 0%d-%d", countryCode, number));
    }
 
    @Override
    public boolean sendSMS(String message) {
        System.out.println(String.format("Samsung can send text message - %s", message));
        return true;
    }
}
