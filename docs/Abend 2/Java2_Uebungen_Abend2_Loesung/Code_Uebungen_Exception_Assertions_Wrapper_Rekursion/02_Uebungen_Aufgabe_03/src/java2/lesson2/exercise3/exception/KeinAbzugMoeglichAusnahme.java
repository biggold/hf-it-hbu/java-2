package java2.lesson2.exercise3.exception;

/**
 * Exception Klasse f�r den Fall, dass kein Abzug m�glich ist.
 * 
 * @author parz
 * @version 1.0
 * 
 */
public class KeinAbzugMoeglichAusnahme extends Exception {

	public KeinAbzugMoeglichAusnahme() {
		super();
	}

	public KeinAbzugMoeglichAusnahme(String msg) {
		super(msg);
	}
}
