package java2.lesson2.exercise3;

import java2.lesson2.exercise3.exception.KeinAbzugMoeglichAusnahme;
import java2.lesson2.exercise3.exception.KeineAuszahlungMoeglichAusnahme;

/**
 * Klasse f�r Berechnungen der 3. S�ule im Kanton Z�rich
 * 
 * @author parz
 * @versio 1.1
 * 
 */
public class Saeule3aZH implements ISaeule3a {

	/**
	 * Konstante Kapital Steuerfuss im Kanton Z�rich
	 */
	private static final double KAPITAL_STEUERFUSS_ZH = 0.072;

	/**
	 * Berechnung der Steuerersparnis, wenn in die S3a einbezahlt wird.
	 * 
	 * @param steuerbaresEinkommen
	 * @param steuerbaresVermoegen
	 * @param einzahlungS3a
	 * @return
	 * @throws KeinAbzugMoeglichAusnahme
	 */
	public int berechneSteuerErsparnis(int steuerbaresEinkommen,
			int steuerbaresVermoegen, int einzahlungS3a)
			throws KeinAbzugMoeglichAusnahme {
		if (steuerbaresEinkommen > 500000 && steuerbaresVermoegen > 1000000) {
			return einzahlungS3a / 3;
		} else if (steuerbaresEinkommen > 500000
				&& steuerbaresVermoegen <= 1000000) {
			return (int) (einzahlungS3a / 3.5);
		} else if (steuerbaresEinkommen > 200000) {
			return (int) (einzahlungS3a / 3.75);
		} else if (steuerbaresEinkommen >= 30000) {
			return (int) (einzahlungS3a / 3.95);
		}
		throw new KeinAbzugMoeglichAusnahme(
				"Einkommen oder Vermoegen ist ungueltig!");
	}

	/**
	 * Berechnung der Kapitalsteuern, wenn das Kapital der S3a ausbezahlt wird.
	 * 
	 * @param kapitalS3a
	 * @return
	 * @throws KeineAuszahlungMoeglichAusnahme
	 */
	public int berechneKapitalSteuern(int kapitalS3a)
			throws KeineAuszahlungMoeglichAusnahme {
		if (kapitalS3a >= 0) {
			double steuern = kapitalS3a * KAPITAL_STEUERFUSS_ZH;
			// Achtung: Rundungsfehler
			return (int) steuern;
		}
		throw new KeineAuszahlungMoeglichAusnahme("Kapital ist ungueltig!");
	}
}
