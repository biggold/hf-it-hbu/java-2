package java2.lesson4.exercise1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;

public class SchreibenLesenZufallszahl {

	public static void main(String[] args) {
		try {
			String filename = createAndSaveRandomNumbers();
			readAndCalculateNumbers(filename);
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
	}

	private static String createAndSaveRandomNumbers() throws IOException {

		String filename = "." + File.separator + "Zufall.txt";
		int numberOfElements = (int) (10 * Math.random()+1);

		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(
				filename)));

		for (int i = 0; i < numberOfElements; i++) {
			// jede Zahl wird auf eine eigene Zeile geschrieben
			// man koennte auch alle Zahlen in eine Zeile schreiben und mit
			// Leerschlag trennen
			Double randomValue = Math.random() * 500;
			pw.println(randomValue);
		}

		pw.close();

		return filename;
	}

	private static void readAndCalculateNumbers(String filename) {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(filename));
			String line;
			// alle Werte in eine Collection lesen
			// die Anzahl Werte ist nicht bekannt
			Collection<Double> numbers = new ArrayList<>();
			while ((line = in.readLine()) != null) {
				numbers.add(Double.valueOf(line));
			}
			
			System.out.println("Summe: " + calculateSum(numbers));
			System.out.println("Durchschnitt: " + calculateAverage(numbers));
		} catch (FileNotFoundException ex) {
			System.out.println(ex.getMessage());
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		} finally {
			try {
				in.close();
			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
		}
	}
	
	private static double calculateSum(Collection<Double> numbers) {
		double sum = 0;
		for (Double number : numbers) {
			sum += number;
		}
		return sum;
	}

	private static double calculateAverage(Collection<Double> numbers) {
		double sum = calculateSum(numbers);
		if (!numbers.isEmpty()) {
			return sum / numbers.size();
		}
		return 0.0;

	}
}
