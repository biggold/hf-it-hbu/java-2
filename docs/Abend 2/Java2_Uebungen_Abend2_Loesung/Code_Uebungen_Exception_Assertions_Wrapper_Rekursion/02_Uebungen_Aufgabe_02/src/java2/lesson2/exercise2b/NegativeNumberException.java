package java2.lesson2.exercise2b;

public class NegativeNumberException extends Exception {

	public NegativeNumberException() {
	}

	public NegativeNumberException(String message) {
		super(message);
	}

}
