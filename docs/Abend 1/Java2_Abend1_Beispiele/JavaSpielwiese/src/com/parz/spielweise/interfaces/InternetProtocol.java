package com.parz.spielweise.interfaces;

public interface InternetProtocol {
	void discoverAgent();
	void registerNode();
	void createTunneling();
}
