package java2.lesson1.exercise3;

/**
 * Die Klasse <code>Steuerverwaltung</code> dient zum Berechnen der
 * Steuerersparnisse und der Kapitalsteuern der S�ule 3a.
 * 
 * @author parz
 * @version 1.0
 * 
 */
public class Steuerverwaltung {

	private ISaeule3a saeule3a = null;
	private int steuerbaresEinkommen;
	private int steuerbaresVermoegen;
	private int einzahlungS3a;
	private int kapitalS3a;

	private static final String KANTON_CODE_ZH = "ZH";
	private static final String KANTON_CODE_SG = "SG";

	/**
	 * Konstruktor zum Erzeugen der Steuerverwaltung.
	 * 
	 * @param steuerbaresEinkommen
	 *            Steuerbares Einkommen
	 * @param steuerbaresVermoegen
	 *            Steuerbares Verm�gen
	 * @param einzahlungS3a
	 *            Einzahlung in die 3. S�ule
	 * @param kapitalS3a
	 *            Kapital in der 3. S�ule
	 */
	public Steuerverwaltung(int steuerbaresEinkommen, int steuerbaresVermoegen,
			int einzahlungS3a, int kapitalS3a) {
		this.steuerbaresEinkommen = steuerbaresEinkommen;
		this.steuerbaresVermoegen = steuerbaresVermoegen;
		this.einzahlungS3a = einzahlungS3a;
		this.kapitalS3a = kapitalS3a;
	}

	/**
	 * Setter f�r das Kapital
	 * 
	 * @param kapitalS3a
	 *            Kapital in der 3. S�ule
	 */
	public void setKapitalS3a(int kapitalS3a) {
		this.kapitalS3a = kapitalS3a;
	}

	/**
	 * Setter f�r die Einzahlung
	 * 
	 * @param einzahlungS3a
	 *            Einzahlung in die 3. S�ule
	 */
	public void setEinzahlungS3a(int einzahlungS3a) {
		this.einzahlungS3a = einzahlungS3a;
	}

	/**
	 * Setter f�r das steuerbare Einkommen
	 * 
	 * @param steuerbaresEinkommen
	 *            Steuerbares Einkommen
	 */
	public void setSteuerbaresEinkommen(int steuerbaresEinkommen) {
		this.steuerbaresEinkommen = steuerbaresEinkommen;
	}

	/**
	 * Setter f�r das steuerbare Verm�gen
	 * 
	 * @param steuerbaresVermoegen
	 *            Steuerbares Verm�gen
	 */
	public void setSteuerbaresVermoegen(int steuerbaresVermoegen) {
		this.steuerbaresVermoegen = steuerbaresVermoegen;
	}

	/**
	 * Getter f�r die Steuerersparnis
	 * 
	 * @return Steuerersparnis
	 */
	public int getSteuerersparnisS3a() {
		if (saeule3a != null) {
			return saeule3a.berechneSteuerErsparnis(steuerbaresEinkommen,
					steuerbaresVermoegen, einzahlungS3a);
		}
		return 0;
	}

	/**
	 * Getter f�r die Kapitalsteuer
	 * 
	 * @return Kapitalsteuer
	 */
	public int getKapitalSteuer() {
		if (saeule3a != null) {
			return saeule3a.berechneKapitalSteuern(kapitalS3a);
		}
		return 0;
	}

	/**
	 * Setzt den Kanton, f�r den die Berechnung erfolgen soll
	 * 
	 * @param kantonCode
	 *            Code des Kanton, z.B. ZH oder SG
	 */
	public void definiereKanton(String kantonCode) {
		if (kantonCode != null && kantonCode.equals(KANTON_CODE_ZH)) {
			saeule3a = new Saeule3aZH();
		} else if (kantonCode != null && kantonCode.equals(KANTON_CODE_SG)) {
			saeule3a = new Saeule3aSG();
		}
	}

	public static void main(String[] args) {

		Steuerverwaltung steuerverwaltung = new Steuerverwaltung(600000,
				500000, 6000, 850000);
		steuerverwaltung.definiereKanton(KANTON_CODE_ZH);
		int ersparnis = steuerverwaltung.getSteuerersparnisS3a();
		int kapitalSteuer = steuerverwaltung.getKapitalSteuer();
		System.out.println("Steuerersparnis ZH 1. Fall: " + ersparnis
				+ ", Kapitalsteuer: " + kapitalSteuer);
		steuerverwaltung = new Steuerverwaltung(100000, 100000, 6670, 700000);
		steuerverwaltung.definiereKanton(KANTON_CODE_ZH);
		ersparnis = steuerverwaltung.getSteuerersparnisS3a();
		kapitalSteuer = steuerverwaltung.getKapitalSteuer();
		System.out.println("Steuerersparnis ZH 2. Fall: " + ersparnis
				+ ", Kapitalsteuer: " + kapitalSteuer);
		steuerverwaltung = new Steuerverwaltung(30000, 0, 4000, 655000);
		steuerverwaltung.definiereKanton(KANTON_CODE_ZH);
		ersparnis = steuerverwaltung.getSteuerersparnisS3a();
		kapitalSteuer = steuerverwaltung.getKapitalSteuer();
		System.out.println("Steuerersparnis ZH 3. Fall: " + ersparnis
				+ ", Kapitalsteuer: " + kapitalSteuer);
		steuerverwaltung = new Steuerverwaltung(400000, 20000, 5500, 390000);
		steuerverwaltung.definiereKanton(KANTON_CODE_ZH);
		ersparnis = steuerverwaltung.getSteuerersparnisS3a();
		kapitalSteuer = steuerverwaltung.getKapitalSteuer();
		System.out.println("Steuerersparnis ZH 4. Fall: " + ersparnis
				+ ", Kapitalsteuer: " + kapitalSteuer);

		steuerverwaltung = new Steuerverwaltung(600000, 500000, 6000, 850000);
		steuerverwaltung.definiereKanton(KANTON_CODE_SG);
		ersparnis = steuerverwaltung.getSteuerersparnisS3a();
		kapitalSteuer = steuerverwaltung.getKapitalSteuer();
		System.out.println("Steuerersparnis SG 1. Fall: " + ersparnis
				+ ", Kapitalsteuer: " + kapitalSteuer);
		steuerverwaltung = new Steuerverwaltung(100000, 100000, 6670, 700000);
		steuerverwaltung.definiereKanton(KANTON_CODE_SG);
		ersparnis = steuerverwaltung.getSteuerersparnisS3a();
		kapitalSteuer = steuerverwaltung.getKapitalSteuer();
		System.out.println("Steuerersparnis SG 2. Fall: " + ersparnis
				+ ", Kapitalsteuer: " + kapitalSteuer);

	}
}
