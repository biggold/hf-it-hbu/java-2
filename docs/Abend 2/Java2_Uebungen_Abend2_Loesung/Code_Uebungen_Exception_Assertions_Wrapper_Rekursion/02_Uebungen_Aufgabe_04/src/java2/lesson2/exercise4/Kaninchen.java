package java2.lesson2.exercise4;

public class Kaninchen {

	public static long nextGen(int n) {
		if (n < 2) {
			return 1;
		} else {
			return nextGen(n - 1) + nextGen(n - 2);
		}
	}

	public static void main(String[] args) {

		for (int i = 0; i <= 12; i++) {
			System.out.println("Kaninchenpaar (Monat: " + i + ") : "
					+ Kaninchen.nextGen(i));
		}
	}
}
