package java2.lesson3.exercise2.solution;

public class KoordinatenTest {

	public static void main(String[] args) {

		Koordinate<Integer> intKoordinate1 = new Koordinate<>(5, 10);
		Koordinate<Integer> intKoordinate2 = new Koordinate<>(7, 12);

		Koordinate<Float> floatKoordinate1 = new Koordinate<>(3.3f, 6.6f);
		Koordinate<Float> floatKoordinate2 = new Koordinate<>(4.85f, 11.72f);

		Koordinate<Double> doubleKoordinate1 = new Koordinate<>(2.43, 12.98);
		Koordinate<Double> doubleKoordinate2 = new Koordinate<>(9.65, 19.1);

		System.out.println("Integer Koordinate 1: " + intKoordinate1);
		System.out.println("Integer Koordinate 2: " + intKoordinate2);

		System.out.println("Float Koordinate 1: " + floatKoordinate1);
		System.out.println("Float Koordinate 2: " + floatKoordinate2);

		System.out.println("Double Koordinate 1: " + doubleKoordinate1);
		System.out.println("Double Koordinate 2: " + doubleKoordinate2);
	}

}
