package com.parz.spielweise.abstract_classes;

public interface MobilePhone {
    void makeACall(Long number, Integer countryCode);
    boolean sendSMS(String message);
}
