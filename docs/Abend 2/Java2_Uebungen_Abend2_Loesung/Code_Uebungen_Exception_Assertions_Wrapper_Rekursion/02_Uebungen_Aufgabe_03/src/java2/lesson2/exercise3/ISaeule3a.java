package java2.lesson2.exercise3;

import java2.lesson2.exercise3.exception.KeinAbzugMoeglichAusnahme;
import java2.lesson2.exercise3.exception.KeineAuszahlungMoeglichAusnahme;

public interface ISaeule3a {

	/**
	 * Berechnung der Steuerersparnis, wenn in die S3a einbezahlt wird.
	 * 
	 * @param steuerbaresEinkommen
	 * @param steuerbaresVermoegen
	 * @param einzahlungS3a
	 * @return
	 * @throws KeinAbzugMoeglichAusnahme
	 */
	int berechneSteuerErsparnis(int steuerbaresEinkommen,
			int steuerbaresVermoegen, int einzahlungS3a)
			throws KeinAbzugMoeglichAusnahme;

	/**
	 * Berechnung der Kapitalsteuern, wenn das Kapital der S3a ausbezahlt wird.
	 * 
	 * @param kapitalS3a
	 * @return
	 * @throws KeineAuszahlungMoeglichAusnahme
	 */
	int berechneKapitalSteuern(int kapitalS3a)
			throws KeineAuszahlungMoeglichAusnahme;

}
