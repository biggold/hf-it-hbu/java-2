package java2.lesson2.exercise2b;

public class Operationen {

	public static double wurzel(double x) throws NegativeNumberException {

		if (x < 0) {
			throw new NegativeNumberException();
		}
		return Math.sqrt(x);
	}

	public static void main(String[] args) {
		try {
			wurzel(-3.4);
		} catch (NegativeNumberException e) {
			System.out.println("Wert ist negativ");
		}

	}

}
