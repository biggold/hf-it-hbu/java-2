package java2.lesson3.exercise1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class AutoMarken {

	private static List<String> autoMarken = new ArrayList<String>();

	private static void initAutoMarken() {
		autoMarken.add("Audi");
		autoMarken.add("Nissan");
		autoMarken.add("Kia");
		autoMarken.add("Porsche");
		autoMarken.add("Toyota");
		autoMarken.add("BMW");
	}

	public static void main(String[] args) {
		initAutoMarken();
		pruefeAutoMarken("Kia");
		pruefeAutoMarken("Mercedes");
		pruefeAutoMarken("Audi");
		int size = autoMarken.size();
		System.out.println("Laenge der Liste: " + size + " Elemente");
		autoMarken.remove(size - 1);
		size = autoMarken.size();
		System.out.println("Laenge der Liste ist nur noch: " + size
				+ " Elemente");
		int startIndex = 0;
		// endIndex muss um 1 groesser sein, weil der endIndex selbst nicht mehr zaehlt
		int endIndex = 2; 
		List<String> subList = generiereSubListe(startIndex, endIndex);

		System.out.println("**** Iteration mit Iterator durch die Liste ****");
		Iterator<String> iter = subList.iterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		}

		System.out.println("**** Iteration mit foreach durch die Liste ****");
		for (String auto : subList) {
			System.out.println(auto);
		}
		// Alternative
		System.out.println("**** Automatische Iteration durch die Liste (toString() der Liste) ****");
		System.out.println(subList);
	}

	private static void pruefeAutoMarken(String auto) {
		if (autoMarken.contains(auto)) {
			System.out.println("Marke " + auto + " ist in der Liste");
		} else {
			System.out.println("Marke " + auto + " ist nicht in der Liste");
		}

	}

	private static List<String> generiereSubListe(int startIndex, int endIndex) {
		if ((startIndex <= endIndex) && (startIndex >= 0)
				&& (autoMarken != null) && (endIndex < (autoMarken.size() - 1))) {
			List<String> subList = autoMarken.subList(startIndex, endIndex);
			return subList;
		}
		return Collections.emptyList();
	}
}
