package java2.lesson1.exercise3;

/**
 * Klasse f�r Berechnungen der 3. S�ule im Kanton St. Gallen
 * 
 * @author parz
 * @version 1.0
 * 
 */
public class Saeule3aSG implements ISaeule3a {

	/**
	 * Konstante Kapital Steuerfuss im Kanton Z�rich
	 */
	private static final double KAPITAL_STEUERFUSS_SG = 0.0875;

	/**
	 * Berechnung der Steuerersparnis, wenn in die S3a einbezahlt wird.
	 */
	public int berechneSteuerErsparnis(int steuerbaresEinkommen,
			int steuerbaresVermoegen, int einzahlungS3a) {
		if (steuerbaresEinkommen > 0 && steuerbaresVermoegen > 0) {
			return (int) (einzahlungS3a / 3.9);
		} else {
			return 0;
		}
	}

	/**
	 * Berechnung der Kapitalsteuern, wenn das Kapital der S3a ausbezahlt wird.
	 */
	public int berechneKapitalSteuern(int kapitalS3a) {
		if (kapitalS3a >= 0) {
			return (int) (kapitalS3a * KAPITAL_STEUERFUSS_SG);
		} else {
			return 0;
		}
	}
}
