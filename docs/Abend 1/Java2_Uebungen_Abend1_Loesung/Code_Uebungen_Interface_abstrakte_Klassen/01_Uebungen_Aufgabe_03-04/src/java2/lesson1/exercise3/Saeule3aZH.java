package java2.lesson1.exercise3;

/**
 * Klasse für Berechnungen der 3. S�ule im Kanton Zürich
 * 
 * @author parz
 * @version 1.0
 * 
 */
public class Saeule3aZH implements ISaeule3a {

	/**
	 * Konstante Kapital Steuerfuss im Kanton Z�rich
	 */
	private static final double KAPITAL_STEUERFUSS_ZH = 0.072;

	/**
	 * Berechnung der Steuerersparnis, wenn in die S3a einbezahlt wird.
	 */
	public int berechneSteuerErsparnis(int steuerbaresEinkommen,
			int steuerbaresVermoegen, int einzahlungS3a) {
		if (steuerbaresEinkommen > 500000 && steuerbaresVermoegen > 1000000) {
			return einzahlungS3a / 3;
		} else if (steuerbaresEinkommen > 500000
				&& steuerbaresVermoegen <= 1000000) {
			return (int) (einzahlungS3a / 3.5);
		} else if (steuerbaresEinkommen > 200000) {
			return (int) (einzahlungS3a / 3.75);
		} else if (steuerbaresEinkommen >= 30000) {
			return (int) (einzahlungS3a / 3.95);
		} else {
			return 0;
		}
	}

	
	/**
	 * Berechnung der Kapitalsteuern, wenn das Kapital der S3a ausbezahlt wird.
	 */
	public int berechneKapitalSteuern(int kapitalS3a) {
		if (kapitalS3a >= 0) {
			double steuern = kapitalS3a * KAPITAL_STEUERFUSS_ZH;
			// Achtung: Rundungsfehler
			return (int) steuern;
		} else {
			return 0;
		}
	}
}
