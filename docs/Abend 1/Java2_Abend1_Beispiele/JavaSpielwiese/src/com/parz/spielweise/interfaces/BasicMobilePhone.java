package com.parz.spielweise.interfaces;

import java.io.Serializable;

public class BasicMobilePhone implements MobilePhone, Serializable, InternetProtocol {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    public void makeACall(Long number, Integer countryCode) {
        System.out.println(String.format("I can call to number %d-%d", countryCode, number));
    }
 
    @Override
    public boolean sendSMS(String message) {
        System.out.println("I can send SMS with Basic Mobile Phone");
        return true;
    }

	@Override
	public void discoverAgent() {
		System.out.println("I have discovered the Agent for the Basic Mobile Phone");	
	}

	@Override
	public void registerNode() {
		System.out.println("I have registered the Node for the Basic Mobile Phone");	
	}

	@Override
	public void createTunneling() {
		System.out.println("I have created the Tunneling for the Basic Mobile Phone");	
	}
	
    public void supportClassicGames() {
        System.out.println("I can support classic games");
    }

}
