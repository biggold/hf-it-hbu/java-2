import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * Klasse zum Lesen und Schreiben von Notiz Objekten
 * @author parz
 *
 */
public class Notizen {

	/**
	 * Schreibt ein Notiz-Objekt
	 */
	public static void notizSchreiben() {
		Notiz notiz = new Notiz("Fritz", "Das ist eine Notiz");

		try {
			OutputStream dateiSchreiber = new FileOutputStream("notiz.data");
			ObjectOutputStream objektSchreiber = new ObjectOutputStream(
					dateiSchreiber);
			objektSchreiber.writeObject(notiz);
			objektSchreiber.flush();
			objektSchreiber.close();
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
		}
	}

	/**
	 * Liest ein ein Notiz-Objekt
	 */
	public static void notizLesen() {

		try {
			InputStream dateiLeser = new FileInputStream("notiz.data");
			ObjectInputStream objektLeser = new ObjectInputStream(
					dateiLeser);
			Notiz geleseneNotiz=(Notiz)objektLeser.readObject();
			System.out.println(geleseneNotiz.getAutor());
			System.out.println(geleseneNotiz.getNachricht());
			objektLeser.close();
		} catch (IOException | ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) {
		Notizen.notizSchreiben();
		Notizen.notizLesen();
	}
}
