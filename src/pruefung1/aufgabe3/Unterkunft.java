package pruefung1.aufgabe3;

public abstract class Unterkunft {
    private String ort;

    public Unterkunft(String ort) {
        this.ort = ort;
    }

    public String getOrt() {
        return ort;
    }

    public abstract void uebernachten();
}
