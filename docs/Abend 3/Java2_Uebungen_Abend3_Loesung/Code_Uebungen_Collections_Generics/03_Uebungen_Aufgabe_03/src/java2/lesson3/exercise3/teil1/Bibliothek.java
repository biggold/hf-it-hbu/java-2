package java2.lesson3.exercise3.teil1;

import java.util.ArrayList;
import java.util.Collections;


// Aufgaben a) & b)
public class Bibliothek {

	ArrayList<String> buecherListe = new ArrayList<String>();

	public Bibliothek() {

	}

	public void init() {
		buecherListe.add("The Lord of the Rings");
		buecherListe.add("The Hitchhiker's Guide to the Galaxy");
		buecherListe.add("Ichi kyu hachi yon");
		buecherListe.add("Der Hundertjährige, der aus dem Fenster stieg und verschwand");
		buecherListe.add("Head First Java");
	}

	public void addBuch(String buch) {
		buecherListe.add(buch);
	}

	public void printListe() {
		for(String buch:buecherListe){
			System.out.println(buch);
		}
	}
	
	public void sortiereBuecher() {
		Collections.sort(buecherListe);
	}

	public static void main(String[] args) {
		Bibliothek bibliothek = new Bibliothek();
		bibliothek.init();
		bibliothek.addBuch("Harry Potter");
		bibliothek.sortiereBuecher();
		bibliothek.printListe();
	}

}
