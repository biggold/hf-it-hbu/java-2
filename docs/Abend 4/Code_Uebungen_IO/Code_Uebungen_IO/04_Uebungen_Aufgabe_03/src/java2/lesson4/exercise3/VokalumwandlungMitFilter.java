package java2.lesson4.exercise3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class VokalumwandlungMitFilter {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.print("Name der Input Datei: ");
		String inputFileName = sc.next();
		System.out.print("Vokal: ");
		char vowel = sc.next().charAt(0);
		sc.close();

		File inputFile, outputFile;
		BufferedReader br = null;
		PrintWriter pw = null;
		
		try {
			inputFile = new File(inputFileName);
			outputFile = new File(createOutputFilename(inputFileName));
			br = new BufferedReader(new FileReader(inputFile));
			pw = new PrintWriter(new BufferedWriter(new VowelFilterWriter(
					new FileWriter(outputFile), vowel)));
			
			String line;
			while ((line = br.readLine()) != null) {
				pw.println(line);
			}
		} catch (FileNotFoundException fnfe) {
			System.out.println("Die Datei " + inputFileName
					+ " existiert nicht");
			return;
		} catch (IOException e) {
			System.out.println(e);
			return;
		} finally {
			try {
				if (pw != null)
					pw.close();
				if (br != null)
					br.close();
			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
		}
		
		printFile(outputFile);

	}

	// appends the string _output to the filename
	private static String createOutputFilename(String inputFilename) {
		return filename(inputFilename) + "_output." + extension(inputFilename);
	}

	// gets the file extension
	private static String extension(String file) {
		int dot = file.lastIndexOf(".");
		return file.substring(dot + 1);
	}

	// gets filename without extension
	private static String filename(String file) { 
		int dot = file.lastIndexOf(".");
		int sep = file.lastIndexOf(File.separator);
		return file.substring(sep + 1, dot);
	}

	// dumps the content of a file to the console
	private static void printFile(File file) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			System.out.println(e);
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				System.out.println(ex.getMessage());
			}
		}
	}

}
