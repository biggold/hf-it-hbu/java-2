package java2.lesson4.exercise3;

import java.io.FilterWriter;
import java.io.IOException;
import java.io.Writer;

public class VowelFilterWriter extends FilterWriter {

	private static char[] aeiouLower = { 'a', 'e', 'i', 'o', 'u' };
	private static char[] aeiouUpper = { 'A', 'E', 'I', 'O', 'U' };

	private char vowel = 0;

	public VowelFilterWriter(Writer out, char vowel) {
		super(out);
		this.vowel = vowel;
	}

	public void write(int c) throws IOException {
		super.write(replaceVowels((char) c, vowel));
	}

	public void write(char[] cbuf, int off, int len) throws IOException {
		for (int i = 0; i < len; ++i) {
			write(cbuf[off + i]);
		}
	}

	public void write(String str, int off, int len) throws IOException {
		write(str.toCharArray(), off, len);
	}

	private static char replaceVowels(char c, char vowel) {
		char vowelLower = Character.toLowerCase(vowel);
		char vowelUpper = Character.toUpperCase(vowel);
		for (int i = 0; i < aeiouLower.length; i++) {
			if (c == aeiouLower[i]) {
				c = vowelLower;
			}
			if (c == aeiouUpper[i]) {
				c = vowelUpper;
			}
		}

		return c;
	}

}
