package com.parz.spielweise.interfaces;

public class InterfaceDemo {

    public static void main(String[] args) {
        MobilePhone basicMobilePhone = new BasicMobilePhone();
        MobilePhone smartPhone = new SmartPhone();
 
        System.out.println("Basic Mobile Phone features");
        basicMobilePhone.makeACall(1234567890L, 91);
        basicMobilePhone.sendSMS("Hello world!!!");
        ((BasicMobilePhone) basicMobilePhone).discoverAgent();
        ((BasicMobilePhone) basicMobilePhone).registerNode();
        ((BasicMobilePhone) basicMobilePhone).createTunneling();
        ((BasicMobilePhone) basicMobilePhone).supportClassicGames();
        System.out.println("-------------------------------------------------");
        System.out.println("Smart Phone features");
        smartPhone.makeACall(1234567890L, 91);
        smartPhone.sendSMS("Hello world!!!");
        ((SmartPhone) smartPhone).discoverAgent();
        ((SmartPhone) smartPhone).registerNode();
        ((SmartPhone) smartPhone).createTunneling();
        ((SmartPhone) smartPhone).canCapturePhoto();
        ((SmartPhone) smartPhone).supportApps();
        ((SmartPhone) smartPhone).switchOnBluetooth();
        ((SmartPhone) smartPhone).switchOffBluetooth();
        ((SmartPhone) smartPhone).switchOnWiFi();
        ((SmartPhone) smartPhone).switchOffWiFi();
        System.out.println("-------------------------------------------------");
    }

}
