package pruefung1.aufgabe3;

public class Ferien {
    public static void main(String[] args) {
        Unterkunft[] unterkuenfte = new Unterkunft[6];
        unterkuenfte[0] = new Sheraton("Tokio", 4);
        unterkuenfte[1] = new Sheraton("Teneriffa", 5);
        unterkuenfte[2] = new Hilton("New York", 5);
        unterkuenfte[3] = new Hilton("London", 4);
        unterkuenfte[4] = new Appartement("San Diego", "Sunstar", 17);
        unterkuenfte[5] = new Appartement("Sarasota", "Sandy-Beach", 3);
        uebernachten(unterkuenfte);
    }

    public static void uebernachten(Unterkunft[] unterkuenfte) {
        // hier muss Code eingefügt werden
        // zeige die Ausgabe der Übernachtung für jede Unterkunft
        for (Unterkunft unterkunft : unterkuenfte) {
            unterkunft.uebernachten();
        }
    }
}
