package java2.lesson2.exercise1;

public class Quotient {

	private static int quotient(String zahl1, String zahl2)
			throws ArithmeticException {
		try {
			return Integer.parseInt(zahl1) / Integer.parseInt(zahl2);
		} catch (NumberFormatException e) {
			System.out.println("Falsches Format");
			return 0;
		} finally {
			System.out.println("Danke!");
		}
	}

	public static void main(String[] args) {
		try {
			
			String s1 = "drei", s2 = "0";
			
			System.out.println("Quotient = " + quotient(s1, s2));
		} catch (ArithmeticException e) {
			System.out.println("Nicht durch 0 dividieren!");
		}
	}
}

