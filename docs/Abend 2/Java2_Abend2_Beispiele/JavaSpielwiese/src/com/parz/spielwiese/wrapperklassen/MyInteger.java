package com.parz.spielwiese.wrapperklassen;

public class MyInteger {
	
	int a;
	
	public MyInteger(int a) {
		this.a = a;
	}
	
	public String toString() {
		return "My Integer{a= " + a + "}";
	}

}
