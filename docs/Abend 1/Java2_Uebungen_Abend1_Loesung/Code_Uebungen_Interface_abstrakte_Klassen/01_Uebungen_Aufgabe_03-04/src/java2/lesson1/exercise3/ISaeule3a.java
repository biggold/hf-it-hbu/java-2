
package java2.lesson1.exercise3;



public interface ISaeule3a {

    /**
     * Berechnung der Steuerersparnis, wenn in die S3a einbezahlt wird.
     * @param steuerbaresEinkommen
     * @param steuerbaresVermoegen
     * @param einzahlungS3a
     * @return Steuerersparnis
     * @throws KeinAbzugMoeglichAusnahme
     */
    int berechneSteuerErsparnis(int steuerbaresEinkommen, int steuerbaresVermoegen, int einzahlungS3a);

    /**
     * Berechnung der Kapitalsteuern, wenn das Kapital der S3a ausbezahlt wird.
     * @param kapitalS3a
     * @return Kapitalsteuern
     * @throws KeineAuszahlungMoeglichAusnahme
     */
    int berechneKapitalSteuern(int kapitalS3a);
    

}
