package java2.lesson2.exercise2a;

public class ArrayAccess {

	public static int accessArray(int[] array, int index) {

		int tmp = -1;

		try {
			tmp = array[index];
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("index "+index+" liegt ausserhalb der Array-Grenze");
			e.printStackTrace();
			
		}

		return tmp;
	}

	public static void main(String[] args) {
		int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

		accessArray(array, 11);
	}

}
