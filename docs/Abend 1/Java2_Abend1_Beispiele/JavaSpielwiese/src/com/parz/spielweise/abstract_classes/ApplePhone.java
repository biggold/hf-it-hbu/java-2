package com.parz.spielweise.abstract_classes;

public class ApplePhone implements MobilePhone {
    public final void displayPhoneOSType() {
        System.out.println("I run on iOS");
    }
    
    public final void supportiOSApps() {
        System.out.println("I support iOS Apps (File Format .ipa)");
    }
 
    public void capturePhoto() {
    	System.out.println("Apple Smartphone can capture photo");
    }

    @Override
    public void makeACall(Long number, Integer countryCode) {
        System.out.println(String.format("Apple can make audio and video calls to 0%d-%d", countryCode, number));
    }
 
    @Override
    public boolean sendSMS(String message) {
        System.out.println(String.format("Apple can send text message - %s", message));
        return true;
    }
}
