package com.parz.spielweise.interfaces;

public interface Bluetooth {
	void switchOnBluetooth();
	void switchOffBluetooth();
}
