package java2.lesson2.exercise5;


public class Hanoi {

	public static void hanoi(int k, char start, char ziel, char hilfe) {
		if (k > 0) {
			hanoi(k - 1, start, hilfe, ziel);
			System.out.print("Versetze Scheibe " + k);
			System.out.print(" von " + start);
			System.out.println(" nach " + ziel);
			hanoi(k - 1, hilfe, ziel, start);
		}

	}

	public static void main(String[] args) {

		System.out.println("\nT�rme von Hanoi mit drei Scheiben:");
		Hanoi.hanoi(3, 'A', 'B', 'C');

		System.out.println("\nT�rme von Hanoi mit vier Scheiben:");
		Hanoi.hanoi(4, 'A', 'B', 'C');

		System.out.println("\nT�rme von Hanoi mit f�nf Scheiben:");
		Hanoi.hanoi(5, 'A', 'B', 'C');
	}
}
